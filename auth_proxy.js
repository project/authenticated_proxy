
(function ($) {

Drupal.AuthProxy = Drupal.AuthProxy || {};

/**
 * Send an ajax request via our Node.js authenticated proxy.
 */
Drupal.AuthProxy.ajax = function (url, callback, data) {
  var fullUrl = Drupal.settings.basePath + Drupal.settings.auth_proxy.baseUrl + '/' + url;
  $.ajax({
    type: 'GET',
    url: fullUrl,
    data: data || {},
    dataType: 'json',
    success: callback,
    beforeSend: function(xhr) {
      // No jQuery 1.5 by default in D7, so we can't use headers.
      xhr.setRequestHeader('authproxytoken', Drupal.settings.nodejs.authToken);
    },
    error: function (xmlhttp) {
      alert(Drupal.ajaxError(xmlhttp));
    }
  });
};

/**
 * Just a debug handler.
 */
Drupal.AuthProxy.requestHandler = function (data, status, jqXHR) {
  console.log(data);
};

})(jQuery);

// vi:ai:expandtab:sw=2 ts=2

