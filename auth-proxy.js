/**
 * Authenticated proxy extension for Drupal Node.js server.
 */
var publishMessageToClient,
    settings = require('./auth-proxy.config.js').settings,
    urlMap = settings.urlMap || false,
    backendPath = '',
    request,
    authenticatedClients;

var getBackendBaseUrl = function(settings) {
  var baseUrl = settings.scheme + '://' + settings.host;
  if (settings.scheme == 'http' && settings.port != 80) {
    baseUrl += ':' + settings.port.toString();
  }
  else if (settings.scheme == 'https' && settings.port != 443) {
    baseUrl += ':' + settings.port.toString();
  }
  baseUrl += settings.backendBaseUrl;
  return baseUrl;
}

var handleProxyRequests = function(req, res) {
  var user = authenticatedClients[req.header('authproxytoken')];
  if (user) {

    var originPathParts = req.params[0].split('/').filter(function(b) { b != ''}) || [];
    if (urlMap) {
      var urlMapKey = originPathParts.shift();
      if (!urlMap[urlMapKey]) {
	res.send({'status': 'invalid url'});
	return;
      }

      backendPathParts = urlMap[urlMapKey].path.split('/');
      for (var i = 0; i < backendPathParts; i++) {
	if (backendPathParts[i] == ':uid') {
	  backendPathParts[i] = user.uid;
	}
      }
      if (urlMap[urlMapKey].appendOriginPath) {
        backendPathParts.concat(originPathParts);
      }
      backendPath = backendPathParts.join('/');
    }
    else {
      backendPath = req.params[0];
    }

    var query = req.query;
    if (settings.uidQueryKey) {
      query[settings.uidQueryKey] = user.uid;
    }

    var options = {
      url: getBackendBaseUrl(settings) + backendPath,
      qs: query
    };
    if (settings.debug) {
      console.log("Sending request to backend with options", options);
    }
    req.pipe(request(options)).pipe(res);
  }
  else {
    if (settings.debug) {
      console.log("Auth token failure, header was", req.header('authproxytoken'));
    }
    res.send({'status': 'failed authtoken'});
  }
}

exports.setup = function (config) {
  authenticatedClients = config.authenticatedClients;
  request = config.request;
};

exports.routes = [
  {path: settings.proxyBaseUrl + '*', handler: handleProxyRequests}
];

// vi:ai:expandtab:sw=2 ts=2

